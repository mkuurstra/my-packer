# My Packer

## Proxmox

- [Creating a Proxmox Virtual Environment token](docs/proxmox-token.md)

## Executing

Create `.env` file with the following variables:

- PKR_VAR_proxmox_api_token_id
- PKR_VAR_proxmox_api_token_secret
- TF_VAR_proxmox_api_token_id
- TF_VAR_proxmox_api_token_secret

```shell
set -o allexport && source .env && set +o allexport
```

### Packer

```shell
packer validate -var-file packer/proxmox.pkrvars.hcl packer/ubuntu-server-22-04
```

```shell
packer build -var-file packer/proxmox.pkrvars.hcl packer/ubuntu-server-22-04
```

### Terraform

```shell
terraform -chdir=terraform/ubuntu-server-22-04-vm plan -var-file=../proxmox.tfvars -var-file=test4.tfvars
```

```shell
terraform -chdir=terraform/ubuntu-server-22-04-vm apply -var-file=../proxmox.tfvars -var-file=test4.tfvars
```
