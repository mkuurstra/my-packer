# Creating a Proxmox Virtual Environment token

1. Go to `Datacenter`
1. Go to `API Tokens` under `Permissions`
1. Click on `Add`

![Proxmox token - Step 1](proxmox-token-1.png)

1. Choose an account with sufficient priviliges
1. Give the token an ID
1. Give the token a description
1. Uncheck `Privilege Seperation` to inherit the priviliges of the selected user

![Proxmox token - Step 2](proxmox-token-2.png)

1. Notice the token ID and save it
1. Do the same for the token secret

![Proxmox token - Step 3](proxmox-token-3.png)
