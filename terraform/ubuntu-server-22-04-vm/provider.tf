# Proxmox Provider
# ---
# Initial Provider Configuration for Proxmox

# Variable Definitions
variable "proxmox_api_url" {
    type = string
    description = "The URL where the Proxmox API is located"
}

variable "proxmox_api_token_id" {
    type = string
    description = "The allocated token ID by Proxmox"
}

variable "proxmox_api_token_secret" {
    type = string
    sensitive = true
    description = "The generated token secret by Proxmox"
}

variable "proxmox_insecure_skip_tls_verify" {
    type = bool
    default = false
    description = "Skip certificate verification on proxmox"
}

terraform {
    required_providers {
        proxmox = {
            source = "telmate/proxmox"
            version = "2.9.14"
        }
    }
}

provider "proxmox" {

    pm_api_url = var.proxmox_api_url
    pm_api_token_id = var.proxmox_api_token_id
    pm_api_token_secret = var.proxmox_api_token_secret
    pm_tls_insecure = var.proxmox_insecure_skip_tls_verify

}
