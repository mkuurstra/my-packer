# Proxmox Full-Clone
# ---
# On Proxmox create an Ubuntu Server VM with k3s agent configured

# Variable Definitions
variable "vm_name" {
    type = string
    description = "Name of the desired VM"
}

variable "proxmox_node" {
    type = string
    description = "Which node in the Proxmox cluster to start the virtual machine on during creation"
}

variable "vlan_tag" {
    type = number
    description = "Vlan ID for the VM to initialize on"
}

variable "ipaddr" {
    type = string
    description = "IPv4 address for the VM in cidr notation"
}

variable "gateway" {
    type = string
    description = "IPv4 gateway for the VM"
}


resource "proxmox_vm_qemu" "ubuntu-server-22-04-vm" {
    
    # VM General Settings
    target_node = var.proxmox_node
    name = var.vm_name
    desc = "Ubuntu Server VM with k3s agent configured"

    # VM Advanced General Settings
    onboot = true 

    # Source template
    clone = "ubuntu-server-22-04"

    # Enable the QEMU Guest Agent
    agent = 1
    
    # VM CPU Settings
    cores = 2
    sockets = 1
    cpu = "host"    
    
    # VM Memory Settings
    memory = 2048

    # VM Network Settings
    network {
        bridge = "vmbr0"
        model  = "virtio"
        tag = var.vlan_tag
    }

    # VM Cloud-Init Settings
    os_type = "cloud-init"

    # IP Address and Gateway
    ipconfig0 = "ip=${var.ipaddr},gw=${var.gateway}"
    nameserver = "192.168.2.10"
    searchdomain = "priv.kuurstra.com"

    # Default User
    ciuser = "marvink"
    
    # Add your SSH KEY
    sshkeys = <<EOF
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINU9ykRSeNYqG2B93g/yPyEPzeqcmdcmXFLMdAzzqNO7 marvink@Marvins-MBP
    EOF

}

resource "null_resource" "post" {
    connection {
        type     = "ssh"
        user     = "automation"
        host     = proxmox_vm_qemu.ubuntu-server-22-04-vm.ssh_host
        private_key = "${file("../../id_ed25519")}"
    }
    provisioner "remote-exec" {
        inline = [
        "ip a"
        ]
    }
}