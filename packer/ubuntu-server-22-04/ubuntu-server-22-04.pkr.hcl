# Ubuntu Server 22.04
# ---
# Packer Template to create an Ubuntu Server on Proxmox

# Variable Definitions
variable "proxmox_api_url" {
    type = string
    description = "The URL where the Proxmox API is located"
}

variable "proxmox_api_token_id" {
    type = string
    description = "The allocated token ID by Proxmox"
}

variable "proxmox_api_token_secret" {
    type = string
    sensitive = true
    description = "The generated token secret by Proxmox"
}

variable "proxmox_insecure_skip_tls_verify" {
    type = bool
    default = false
    description = "Skip certificate verification on proxmox"
}

variable "proxmox_node" {
    type = string
    description = "Which node in the Proxmox cluster to start the virtual machine on during creation"
}

variable "vlan_tag" {
    type = number
    description = "Vlan ID for the VM to initialize on"
    default = 20
}

packer {
  required_plugins {
    name = {
      version = "~> 1"
      source  = "github.com/hashicorp/proxmox"
    }
  }
}

# Resource definition for the VM Template
source "proxmox-iso" "ubuntu-server-22-04" {

    # Proxmox connection settings
    proxmox_url = var.proxmox_api_url
    username = var.proxmox_api_token_id
    token = var.proxmox_api_token_secret
    insecure_skip_tls_verify = var.proxmox_insecure_skip_tls_verify

    # VM general settings
    node = var.proxmox_node
    vm_id = "9000"
    vm_name = "ubuntu-server-22-04"
    template_description = "Ubuntu Server 22.04"

    # VM OS Settings
    # (Option 1) Local ISO File
    iso_file = "synology:iso/ubuntu-22.04.3-live-server-amd64.iso"
    # - or -
    # (Option 2) Download ISO
    # iso_url = "https://releases.ubuntu.com/jammy/ubuntu-22.04.3-live-server-amd64.iso"
    iso_checksum = "a4acfda10b18da50e2ec50ccaf860d7f20b389df8765611142305c0e911d16fd"
    iso_storage_pool = "local"
    unmount_iso = true

    # VM System Settings
    qemu_agent = true

    # VM Hard Disk Settings
    scsi_controller = "virtio-scsi-pci"

    disks {
        disk_size = "40G"
        storage_pool = "local-lvm"
        type = "virtio"
    }

    # VM CPU Settings
    cores = "2"

    # VM Memory Settings
    memory = "2048" 

    # VM Network Settings
    network_adapters {
        model = "virtio"
        bridge = "vmbr0"
        vlan_tag = var.vlan_tag
        firewall = "false"
    } 

    # VM Cloud-Init Settings
    cloud_init = true
    cloud_init_storage_pool = "local-lvm"

    # PACKER Boot Commands
    boot_command = [
        "<esc><wait3s>",
        "e<wait>",
        "<down><down><down><end>",
        "<bs><bs><bs><bs><wait>",
        "autoinstall ds=nocloud-net\\;s=http://{{ .HTTPIP }}:{{ .HTTPPort }}/ ---<wait>",
        "<f10><wait>"
    ]
    boot = "c"
    boot_wait = "5s"

    # PACKER Autoinstall Settings
    http_directory = "${path.root}/http"

    # VM authentication
    ssh_username = "automation"
    ssh_private_key_file = "${path.root}/../../id_ed25519"

    # Raise the timeout, when installation takes longer
    ssh_timeout = "20m"
}

# Build Definition to create the VM Template
build {

    name = "ubuntu-server-22-04"
    sources = ["source.proxmox-iso.ubuntu-server-22-04"]

    # Provisioning the VM Template for Cloud-Init Integration in Proxmox #1
    provisioner "shell" {
        inline = [
            "while [ ! -f /var/lib/cloud/instance/boot-finished ]; do echo 'Waiting for cloud-init...'; sleep 1; done",
            "sudo rm /etc/ssh/ssh_host_*",
            "sudo truncate -s 0 /etc/machine-id",
            "sudo apt -y autoremove --purge",
            "sudo apt -y clean",
            "sudo apt -y autoclean",
            "sudo cloud-init clean",
            "sudo rm -f /etc/cloud/cloud.cfg.d/subiquity-disable-cloudinit-networking.cfg",
            "sudo rm -f /etc/netplan/00-installer-config.yaml",
            "sudo sync"
        ]
    }

    # Provisioning the VM Template for Cloud-Init Integration in Proxmox #2
    provisioner "file" {
        source = "${path.root}/files/99-pve.cfg"
        destination = "/tmp/99-pve.cfg"
    }

    # Provisioning the VM Template for Cloud-Init Integration in Proxmox #3
    provisioner "shell" {
        inline = [ "sudo cp /tmp/99-pve.cfg /etc/cloud/cloud.cfg.d/99-pve.cfg" ]
    }
}
