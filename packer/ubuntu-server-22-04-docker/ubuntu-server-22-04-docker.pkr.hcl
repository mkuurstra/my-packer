# Ubuntu Server 22.04 with Docker
# ---
# Packer Template to create an Ubuntu Server on Proxmox with docker installed

# Variable Definitions
variable "proxmox_api_url" {
    type = string
    description = "The URL where the Proxmox API is located"
}

variable "proxmox_api_token_id" {
    type = string
    description = "The allocated token ID by Proxmox"
}

variable "proxmox_api_token_secret" {
    type = string
    sensitive = true
    description = "The generated token secret by Proxmox"
}

variable "proxmox_insecure_skip_tls_verify" {
    type = bool
    default = false
    description = "Skip certificate verification on proxmox"
}

variable "proxmox_node" {
    type = string
    description = "Which node in the Proxmox cluster to start the virtual machine on during creation"
}

variable "vlan_tag" {
    type = number
    description = "Vlan ID for the VM to initialize on"
    default = 20
}

packer {
  required_plugins {
    name = {
      version = "~> 1"
      source  = "github.com/hashicorp/proxmox"
    }
  }
}

# Resource definition for the VM Template
source "proxmox-clone" "ubuntu-server-22-04-docker" {

    # Proxmox connection settings
    proxmox_url = var.proxmox_api_url
    username = var.proxmox_api_token_id
    token = var.proxmox_api_token_secret
    insecure_skip_tls_verify = var.proxmox_insecure_skip_tls_verify

    # VM clone source
    clone_vm_id = 9000

    # VM general settings
    node = var.proxmox_node
    vm_id = "9001"
    vm_name = "ubuntu-server-22-04-docker"
    template_description = "Ubuntu Server 22.04 with docker installed"

    # VM System Settings
    qemu_agent = true

    # VM Hard Disk Settings
    scsi_controller = "virtio-scsi-pci"

    # VM CPU Settings
    cores = "2"

    # VM Memory Settings
    memory = "2048" 

    # VM Network Settings
    network_adapters {
        model = "virtio"
        bridge = "vmbr0"
        vlan_tag = var.vlan_tag
        firewall = "false"
    } 
    ipconfig {
        ip = "dhcp"
    }

    # VM Cloud-Init Settings
    cloud_init = true
    cloud_init_storage_pool = "local-lvm"

    # VM authentication
    ssh_username = "root"
    # SSH key is automatically generated for use, and is used in the image's Cloud-Init settings for provisioning

    # Raise the timeout, when installation takes longer
    ssh_timeout = "20m"
}

# Build Definition to create the VM Template
build {

    name = "ubuntu-server-22-04-docker"
    sources = ["source.proxmox-clone.ubuntu-server-22-04-docker"]

    # Provisioning the VM Template for Cloud-Init Integration in Proxmox
    provisioner "shell" {
        inline = [
            "while [ ! -f /var/lib/cloud/instance/boot-finished ]; do echo 'Waiting for cloud-init...'; sleep 1; done",
            "sudo rm /etc/ssh/ssh_host_*",
            "sudo truncate -s 0 /etc/machine-id",
            "sudo apt -y autoremove --purge",
            "sudo apt -y clean",
            "sudo apt -y autoclean",
            "sudo cloud-init clean",
            "sudo rm -f /etc/cloud/cloud.cfg.d/subiquity-disable-cloudinit-networking.cfg",
            "sudo rm -f /etc/netplan/00-installer-config.yaml",
            "sudo sync"
        ]
    }

    # Provisioning the VM Template with Docker Installation
    provisioner "shell" {
        inline = [
            "sudo apt-get install -y ca-certificates curl gnupg lsb-release",
            "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg",
            "echo \"deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable\" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null",
            "sudo apt-get -y update",
            "sudo apt-get install -y docker-ce docker-ce-cli containerd.io"
        ]
    }
}
